package ru.russianpost.page;


import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.springframework.stereotype.Service;

import java.util.HashMap;

import static com.codeborne.selenide.Selenide.*;

@Service
public class Login_page implements Page {
    @Override
    public SelenideElement getElements(String element) {
        HashMap<String, SelenideElement> selenideElement = new HashMap<>();
        selenideElement.put("Кнопка Войти портал", $x("//div[@class='Box-sc-7ax6ia-0 cquGUr']"));
        selenideElement.put("Логин", $x("//input[@id='username']"));
        selenideElement.put("пароль",$x("//input[@id='userpassword']"));
        selenideElement.put("Войти",$x("//span[text()='Войти']"));
        selenideElement.put("username",$x("//div[@class='Box-sc-7ax6ia-0 Headerstyles__UserNameContainer-sc-1rbx3hp-9 iztNsN gMVagu']"));
        selenideElement.put("Мои отправления",$x("//a[text()='Мои отправления']"));
        selenideElement.put("текст",$x("//p[@class='Paragraph-sc-10hckd4-0 fhoCWJ']"));

        return selenideElement.get(element);
    }

    @Override
    public ElementsCollection getElementsList(String element) {
        HashMap<String, ElementsCollection> selenideElementHashMap = new HashMap<>();
        selenideElementHashMap.put("", $$x(""));
        selenideElementHashMap.put("", $$x(""));

        return selenideElementHashMap.get(element);
    }

    @Override
    public String getElementsString(String element) {
        return null;
    }
}

