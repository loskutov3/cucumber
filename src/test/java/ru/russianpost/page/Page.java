package ru.russianpost.page;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.Map;

public interface Page {

    SelenideElement getElements(String element);

    ElementsCollection getElementsList(String element);

    String getElementsString(String element);
}
