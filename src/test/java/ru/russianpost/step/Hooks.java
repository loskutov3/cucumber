package ru.russianpost.step;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.sun.istack.NotNull;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import lombok.AllArgsConstructor;
import org.junit.AfterClass;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import ru.russianpost.config.LocalChromeWebdriverBeanConfig;
import ru.russianpost.config.LocalFirefoxlWebdriverBeanConfig;
import ru.russianpost.config.PropertiesConfig;
import ru.russianpost.config.RemoteWebdriverBeanConfig;

import static com.codeborne.selenide.Selenide.open;

@AllArgsConstructor
@ContextConfiguration(classes = {
        LocalChromeWebdriverBeanConfig.class, LocalFirefoxlWebdriverBeanConfig.class,
        RemoteWebdriverBeanConfig.class})
public class Hooks {

    @NotNull
    WebDriver webDriver;

    @Autowired
    PropertiesConfig config;

    @Before
    @Step("start browser session")
    public void before() {
        Configuration.timeout = 15000;
        Configuration.reportsFolder = "target/selenide-screenshots";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
        open(config.getWeb().getBaseurl());
        WebDriverRunner.getWebDriver().manage().window().maximize();
    }

    @After(value = "@tearDown")
    public void ternDown() {
        webDriver.close();
    }


    @After
    @Step("logout")
    public void after(Scenario scenario) {
        if (scenario.isFailed()) {
            // Take a screenshot...
            final byte[] screenshot = ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
        }
    }
}
