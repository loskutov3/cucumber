package ru.russianpost.step;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.То;
import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import ru.russianpost.page.Page;
import ru.russianpost.service.PageMap;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@Slf4j
public class CommonStep {

    @Autowired
    private PageMap pageMap;

    @Step("на странице {page} кликаем на {field}")
    @Если("на странице {string} кликаем на {string}")
    public void clickSaveButton(String page, String field) {
        Page selectedPage = pageMap.getSelectedPage(page);
        selectedPage.getElements(field).click();
    }
    @Если("на странице {string} наводим на {string}")
    public void hoveroveranelement(String page, String field) {
        Page selectedPage = pageMap.getSelectedPage(page);
        selectedPage.getElements(field).hover();
    }

    @Если("на странице {string} для элемента {string} вводим текст {string}")
    public void inputText(String page, String field, String text) {
        pageMap.getSelectedPage(page).getElements(field).setValue(text);
    }

    @То("на странице {string} в поле {string} проверяем текст {string}")
    public void checkText(String page, String field, String expectedText) {
        SelenideElement elements = pageMap.getSelectedPage(page).getElements(field).shouldBe(Condition.visible);
        String actualText = elements.getText();
        assertEquals(actualText, expectedText);
    }

    @Когда("^на вкладке \"([^\"]*)\" из коллекции elementsList \"([^\"]*)\" текст из списка элементов должен отображаться")
    public void elementIsNotClickable(String page, String field, List<String> args) {
        ElementsCollection selectedListOfPage = pageMap.getSelectedPage(page).getElementsList(field);
        for (int i = 0; i < selectedListOfPage.size(); i++) {
            String elemText = selectedListOfPage.get(i).text();

            assertTrue(String.format("===>>> элемент с текстом \"%s\" не совподает со значением из списка", elemText), args.contains(elemText));

        }
    }
}