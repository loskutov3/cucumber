package ru.russianpost;

import com.codeborne.selenide.WebDriverRunner;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "json:target/cucumber1.json"},
        features = "src/test/resources/features/features_1",
        dryRun = false,
        tags = "(not @skip)",
        glue = {"ru.russianpost"}
)
public class Run1Test {

    @AfterClass
    public static void afterClass(){
        WebDriverRunner.getWebDriver().close();
    }
}
