package ru.russianpost.config;

import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;
import org.springframework.test.annotation.DirtiesContext;

import java.net.MalformedURLException;
import java.net.URL;

@Configuration
@ComponentScan(lazyInit = true, basePackages = "ru.russianpost")
@Profile("remote")
@Import({DBBeanConfig.class, PropertiesConfig.class})
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class RemoteWebdriverBeanConfig {


    @Bean
    @Qualifier("webDriver")
    public WebDriver webDriver() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("chrome");
        capabilities.setVersion("103.0.5060.53");
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", false);
        capabilities.setCapability("resolution", "1920x1080");
        capabilities.acceptInsecureCerts();
        capabilities.setAcceptInsecureCerts(true);

        WebDriver driver = new RemoteWebDriver(
                new URL("http://127.0.0.1:4444/wd/hub"),
                capabilities
        );
        com.codeborne.selenide.Configuration.timeout = 7000;
        WebDriverRunner.setWebDriver(driver);
        return WebDriverRunner.getWebDriver();
    }

}