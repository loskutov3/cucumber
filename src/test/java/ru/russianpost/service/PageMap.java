package ru.russianpost.service;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import org.openqa.selenium.NoSuchElementException;
import org.springframework.stereotype.Service;
import ru.russianpost.page.*;

@AllArgsConstructor
@Service
public class PageMap {


    @NotNull
    private Login_page login_page;

    public Page getSelectedPage(String page) throws NoSuchElementException {
        switch (page.toLowerCase()) {
            case "портал":
                return login_page;
            default:
                throw new IllegalStateException("указана несуществующая страница: " + page.toLowerCase());
        }
    }
}
